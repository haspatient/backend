<?php namespace Tests\Repositories;

use App\Models\login;
use App\Repositories\loginRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class loginRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var loginRepository
     */
    protected $loginRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->loginRepo = \App::make(loginRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_login()
    {
        $login = login::factory()->make()->toArray();

        $createdlogin = $this->loginRepo->create($login);

        $createdlogin = $createdlogin->toArray();
        $this->assertArrayHasKey('id', $createdlogin);
        $this->assertNotNull($createdlogin['id'], 'Created login must have id specified');
        $this->assertNotNull(login::find($createdlogin['id']), 'login with given id must be in DB');
        $this->assertModelData($login, $createdlogin);
    }

    /**
     * @test read
     */
    public function test_read_login()
    {
        $login = login::factory()->create();

        $dblogin = $this->loginRepo->find($login->id);

        $dblogin = $dblogin->toArray();
        $this->assertModelData($login->toArray(), $dblogin);
    }

    /**
     * @test update
     */
    public function test_update_login()
    {
        $login = login::factory()->create();
        $fakelogin = login::factory()->make()->toArray();

        $updatedlogin = $this->loginRepo->update($fakelogin, $login->id);

        $this->assertModelData($fakelogin, $updatedlogin->toArray());
        $dblogin = $this->loginRepo->find($login->id);
        $this->assertModelData($fakelogin, $dblogin->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_login()
    {
        $login = login::factory()->create();

        $resp = $this->loginRepo->delete($login->id);

        $this->assertTrue($resp);
        $this->assertNull(login::find($login->id), 'login should not exist in DB');
    }
}
