<?php

namespace App\Http\Controllers\API;

use App\Helpers\JwtAuth;
use App\Http\Requests\API\CreateloginAPIRequest;
use App\Http\Requests\API\UpdateloginAPIRequest;
use App\Models\login;
use App\Repositories\loginRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Http\Resources\loginResource;
use Illuminate\Support\Facades\Validator;
use Response;

/**
 * Class loginController
 * @package App\Http\Controllers\API
 */

class loginAPIController extends AppBaseController
{
    /** @var  loginRepository */
    private $loginRepository;

    public function __construct(loginRepository $loginRepo)
    {
        $this->loginRepository = $loginRepo;
    }

    /**
     * Display a listing of the login.
     * GET|HEAD /logins
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $logins = $this->loginRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse(loginResource::collection($logins), 'Logins retrieved successfully');
    }

    /**
     * Store a newly created login in storage.
     * POST /logins
     *
     * @param CreateloginAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateloginAPIRequest $request)
    {
        $input = $request->all();

        $login = $this->loginRepository->create($input);

        return $this->sendResponse(new loginResource($login), 'Login saved successfully');
    }

    /**
     * Display the specified login.
     * GET|HEAD /logins/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id){
        /** @var login $login */
        $login = $this->loginRepository->find($id);

        if (empty($login)) {
            return $this->sendError('Login not found');
        }

        return $this->sendResponse(new loginResource($login), 'Login retrieved successfully');
    }

    /**
     * Update the specified login in storage.
     * PUT/PATCH /logins/{id}
     *
     * @param int $id
     * @param UpdateloginAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateloginAPIRequest $request)
    {
        $input = $request->all();

        /** @var login $login */
        $login = $this->loginRepository->find($id);

        if (empty($login)) {
            return $this->sendError('Login not found');
        }

        $login = $this->loginRepository->update($input, $id);

        return $this->sendResponse(new loginResource($login), 'login updated successfully');
    }

    /**
     * Remove the specified login from storage.
     * DELETE /logins/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id){
        /** @var login $login */
        $login = $this->loginRepository->find($id);

        if (empty($login)) {
            return $this->sendError('Login not found');
        }

        $login->delete();

        return $this->sendSuccess('Login deleted successfully');
    }



    /**
     * Display a listing of the login.
     * GET|HEAD /logins
     *
     * @param Request $request
     * @return Response
     */
    public function auth(Request $data){
        $validation = Validator::make($data->all(),[
            'correo'=>'required|email',
            'password'=>'required',
        ]);

        if ($validation->fails()) {
            return $this->sendError('Verifique que todos los datos sean Correctos',500);
        }

        $jwtAuth = new JwtAuth();
        $email = $data->correo;
        $password = $data->password;
        $token = $data->get('token',null);
        $data =  $jwtAuth->signup($email,$password,$token);

        if(isset($data['code'])){
            return $this->sendError($data['message'],401);
        }

        return $this->sendResponse($data,'Inicio sesión correctamente');
    }

}
