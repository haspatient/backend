<?php
namespace App\Helpers;

use App\Models\User;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class JwtAuth{

  public function signup($email, $password,$getToken=false)  {

    $user = User::where('email', $email)
    ->first();

    if (!Hash::check($password, $user->password)) {
      return $data = array(
        'code'=> 401,
        'message'=>'Usuario y/o contraseña incorrecto.'
      );
    }

    if ($user != null) {
      $token = array(
        'sub' => $user->id,
        'nombre'=>$user->nombres,
        'app'=>$user->apellido_paterno,
        'curp'=>$user->CURP,
        'empresa'=>$user->empresa_id,
        'apm'=>$user->apellido_materno,
        'email'=>$user->correo,
        'iat' => time(),
        'exp'=>time() + (7*24*60*60)
      );
      $jwt = JWT::encode($token,'humanly_ti_has_key_secret','HS256');
      if ($getToken==false) {
        $user=User::find( $user->id);
        $data = array(
          'user' => $user->makeHidden('password'),
        );
      }else {
        $data = array(
          'jwt' => $jwt,
        );
      }
    }

    return $data;
  }

  public function checkToken($jwt, $getIdentity=null){
    $auth =false;
    try {
      $jwt = str_replace('"','',$jwt);
      $decode = JWT::decode($jwt,'humanly_ti_has_key_secret',['HS256']);
    } catch (\Exception $e) {
      $auth=false;
    }
    if (!empty($decode) && is_object($decode) && isset($decode->sub)) {
      $auth = true;
    }else {
      $auth = false;
    }


    if ($getIdentity) {
      return $decode;
    }
    return $auth;
   }
}
