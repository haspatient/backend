<?php

namespace App\Repositories;

use App\Models\login;
use App\Models\User;
use App\Repositories\BaseRepository;

/**
 * Class loginRepository
 * @package App\Repositories
 * @version February 24, 2021, 2:23 am UTC
*/

class loginRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }
}
